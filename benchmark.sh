#!/bin/bash

frameworks=("numpy" "numba" "pythran" "dace_cpu" "cupy" "dace_gpu")

benchmarks=("azimint_hist" "azimint_naive"
            "cavity_flow" "channel_flow"
            "compute"
            "contour_integral"
            "go_fast"
            "mandelbrot1" "mandelbrot2"
            "scattering_self_energies")

deep_learning=("conv2d_bias" "softmax" "mlp" "lenet" "resnet")

for i in "${benchmarks[@]}"
do
    for j in "${frameworks[@]}"
    do
	    timeout 300s python npbench/benchmarks/$i/$i.py -f $j
    done
done

for i in "${deep_learning[@]}"
do
    for j in "${frameworks[@]}"
    do
	    timeout 300s python npbench/benchmarks/deep_learning/$i/$i.py -f $j
    done
done
