To run with n workers you must run n+2 mpi procs, since dask reserves one proc
for the "Client" (the thing that actually parses the python code) and one for
the "Scheduler".  Each worker has one thread by default.

mpirun --oversubscribe -np 4 python ./polybench_dask.py

will run with two worker threads.

