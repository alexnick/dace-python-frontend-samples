import time
import inspect
import math
import numpy as np
from dask_mpi import initialize
initialize(nthreads=18) #TODO this is Piz Daint specific!
import dask.array as da
import timeit
from dask_image import ndfilters as di
from dask.distributed import Client 
client = Client()

sizes = [
{
  "kernel": "jacobi_2d",
  "arguments" : [{'TSTEPS':1000, 'N':1300, 'datatype': np.float64}],
  "weak_scaling": {"N": lambda n, procs: math.sqrt(procs)*n}
},
#{
#  "kernel": "heat_3d",
#  "arguments" : [{'TSTEPS':100, 'N':40, 'datatype': np.float64}],
#  "weak_scaling": {"N": lambda n, procs: np.cbrt(procs)*n}
#},
{
  "kernel": "jacobi_1d",
  "arguments" : [{'TSTEPS':1000, 'N':24000, 'datatype': np.float64}],
  "weak_scaling": {"N": lambda n, procs: procs*n}
},
{
  "kernel": "gemver",
  "arguments" : [{'N': 5000, 'datatype': np.float64}],
  "weak_scaling": {"N": lambda n, procs : math.sqrt(procs)*n}
},
{
  "kernel": "gemm",
  "arguments":  [{'I':4000, 'J':4600, 'K':2600, 'datatype': np.float64}],
  "weak_scaling": {"I": lambda i, procs : i*np.cbrt(procs), "J": lambda j, procs: j*np.cbrt(procs), "K": lambda k, procs: k*np.cbrt(procs)}
},
{
  "kernel": "k2mm",
  "arguments" : [{'NI':3200, 'NJ': 3600, 'NK': 2200, 'NL': 2400, 'datatype': np.float64}],
  "weak_scaling": {"NI": lambda i, procs : i*np.cbrt(procs), "NJ": lambda j, procs: j*np.cbrt(procs), "NK": lambda k, procs: k*np.cbrt(procs), "NL": lambda l, procs: l*np.cbrt(procs)}
},
{
  "kernel": "k3mm",
  "arguments" : [{'NI':3200, 'NJ': 3600, 'NK': 2000, 'NL': 2200, 'NM':2400, 'datatype': np.float64}],
  "weak_scaling": {"NI": lambda i, procs : i*np.cbrt(procs), "NJ": lambda j, procs: j*np.cbrt(procs), "NK": lambda k, procs: k*np.cbrt(procs), "NL": lambda l, procs: l*np.cbrt(procs), "NM": lambda l, procs: l*np.cbrt(procs)}
},
{
  "kernel": "gesummv",
  "arguments" : [{'N': 11200, 'datatype': np.float64}],
  "weak_scaling": {"N": lambda n, procs : math.sqrt(procs)*n}
},
{
  "kernel": "bicg",
  "arguments" : [{'M': 12500, 'N': 10000, 'datatype': np.float64}],
  "weak_scaling": {"M": lambda m, procs : m*math.sqrt(procs), "N": lambda n, procs: n*math.sqrt(procs)}
},
{
  "kernel": "doitgen",
  "arguments" : [{'NR': 128, 'NQ': 512, 'NP': 512, 'datatype': np.float64}],
  "weak_scaling": {"NR": lambda nr, procs : nr*procs,}
},
{
  "kernel": "atax",
  "arguments": [{'M': 10000, 'N': 12500, 'datatype': np.float64}],
  "weak_scaling": {"M": lambda m, procs : m*math.sqrt(procs), "N": lambda n, procs: n*math.sqrt(procs)}
},
{
  "kernel": "mvt",
  "arguments" : [{'N':11000, 'datatype': np.float64}],
  "weak_scaling": {"N": lambda n, procs : n*math.sqrt(procs)}
},


]


def adjust_size(size, scal_func, comm_size, divisor):
    candidate = size * scal_func(comm_size)
    if candidate // divisor != candidate:
        candidate = np.ceil(candidate / divisor) * divisor
    return int(candidate)



def jacobi_2d_bench(TSTEPS, N, datatype):
    # init
    init_start = time.perf_counter()
    A = da.fromfunction(lambda i, j: i * (j + 2) / N,  shape=(N,N),   dtype=datatype, chunks="auto")
    A.compute()
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    kernel = np.array([[0, 0.2, 0], [0.2, 0.2, 0.2], [0, 0.2, 0]], dtype=np.float32)
    for i in range(2 * TSTEPS):
        A = di.convolve(A, kernel, mode='constant', cval=0.0)
    A.compute()
    compute_end = time.perf_counter()

    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }



def heat_3d_bench(TSTEPS, N, datatype):
    # init
    init_start = time.perf_counter()
    A = da.fromfunction(lambda i,j,k: (i + j + (N - k)) * 10 / N, shape=(N, N, N), dtype=datatype, chunks="auto")
    A.compute()
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    #TODO weights are incorrect
    kernel = np.array([[[0, 0.2, 0], [0.2, 0.2, 0.2], [0, 0.2, 0]], [[0, 0.2, 0], [0.2, 0.2, 0.2], [0, 0.2, 0]], [[0, 0.2, 0], [0.2, 0.2, 0.2], [0, 0.2, 0]]], dtype=np.float32)
    for i in range(2 * TSTEPS):
        A = di.convolve(A, kernel, mode='constant', cval=0.0)
    A.compute()
    compute_end = time.perf_counter()

    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }


def jacobi_1d_bench(TSTEPS, N, datatype):
    # init
    init_start = time.perf_counter()
    A = da.fromfunction(lambda i:  (i + 2) / N,  shape=(N,),   dtype=datatype, chunks="auto")
    A.compute()
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    for t in range(2 * TSTEPS):
        kernel = np.array([1/3, 1/3, 1/3], dtype=np.float32)
        A = di.convolve(A, kernel, mode='constant', cval=0.0)
    A.compute()
    compute_end = time.perf_counter()

    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }


def mvt_bench(N, datatype):
    # init
    init_start = time.perf_counter()
    x1 = da.fromfunction(lambda i: (i % N) / N,        shape=(N,),   dtype=datatype, chunks="auto")
    x2 = da.fromfunction(lambda i: ((i + 1) % N) / N,  shape=(N,),   dtype=datatype, chunks="auto")
    y1 = da.fromfunction(lambda i: ((i + 3) % N) / N,  shape=(N,),   dtype=datatype, chunks="auto")
    y2 = da.fromfunction(lambda i: ((i + 4) % N) / N,  shape=(N,),   dtype=datatype, chunks="auto")
    A = da.fromfunction(lambda i, j: (i * j % N) / N,  shape=(N, N), dtype=datatype, chunks="auto")
    A.compute()
    x1.compute()
    x2.compute()
    y1.compute()
    y2.compute()
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    x1 += A @ y1
    x2 += y2 @ A
    x1.compute()
    x2.compute()
    compute_end = time.perf_counter()

    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }



def k3mm_bench(NI, NJ, NK, NL, NM, datatype):
    # init
    init_start = time.perf_counter()    
    A = da.fromfunction(lambda i, j: ((i * j + 1) % NI) / (5 * NI),        shape=(NI, NK), dtype=datatype, chunks="auto")
    B = da.fromfunction(lambda i, j: ((i * (j + 1) + 2) % NJ) / (5 * NJ),  shape=(NK, NJ), dtype=datatype, chunks="auto")
    C = da.fromfunction(lambda i, j: (i * (j + 3) % NL) / (5 * NL),        shape=(NJ, NM), dtype=datatype, chunks="auto")
    D = da.fromfunction(lambda i, j: ((i * (j + 2) + 2) % NK) / ( 5 * NK), shape=(NM, NL), dtype=datatype, chunks="auto")
    A.compute()
    B.compute()
    C.compute()
    D.compute()
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    res = A @ B @ C @ D
    res.compute()
    compute_end = time.perf_counter()

    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }



def k2mm_bench(NI, NJ, NK, NL, datatype):
    # init
    init_start = time.perf_counter()    
    alpha = datatype(1.5)
    beta = datatype(1.2)
    A = da.fromfunction(lambda i, j: ((i * j + 1) % NI) / NI, shape=(NI, NK), dtype=datatype, chunks="auto")
    B = da.fromfunction(lambda i, j: ((i * j + 1) % NJ) / NJ, shape=(NK, NJ), dtype=datatype, chunks="auto")
    C = da.fromfunction(lambda i, j: ((i * j + 3) % NL) / NL, shape=(NJ, NL), dtype=datatype, chunks="auto")
    D = da.fromfunction(lambda i, j: ((i * j + 2) % NK) / NK, shape=(NI, NL), dtype=datatype, chunks="auto")
    A.compute()
    B.compute()
    C.compute()
    D.compute()
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    D = alpha * A @ B @ C + beta * D
    D.compute()
    compute_end = time.perf_counter()

    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }



def gesummv_bench(N, datatype):
    # init
    init_start = time.perf_counter()    
    alpha = datatype(1.5)
    beta = datatype(1.2)
    A = da.fromfunction(lambda i,j: ((i * j + 1) % N) / N, shape=(N, N), dtype=datatype, chunks="auto")
    B = da.fromfunction(lambda i,j: ((i * j + 2) % N) / N, shape=(N, N), dtype=datatype, chunks="auto")
    x = da.fromfunction(lambda i: (i % N) / N,             shape=(N,),   dtype=datatype, chunks="auto")
    A.compute()
    B.compute()
    x.compute()
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    res = alpha * A @ x + beta * B @ x
    res.compute()
    compute_end = time.perf_counter()

    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }



def gemver_bench(N, datatype):
    # init
    init_start = time.perf_counter()    
    alpha = datatype(1.5)
    beta = datatype(1.2)
    fn = datatype(N)
    A = da.fromfunction(lambda i,j: (i * j % N) / N, shape=(N, N), dtype=datatype, chunks="auto")
    u1 = da.fromfunction(lambda i: i, shape=(N, ), dtype=datatype, chunks="auto")
    u2 = da.fromfunction(lambda i:  ((i + 1) / fn) / 2.0, shape=(N, ), dtype=datatype, chunks="auto")
    v1 = da.fromfunction(lambda i: ((i + 1) / fn) / 4.0, shape=(N, ), dtype=datatype, chunks="auto")
    v2 = da.fromfunction(lambda i: ((i + 1) / fn) / 6.0, shape=(N, ), dtype=datatype, chunks="auto")
    y = da.fromfunction(lambda i: ((i + 1) / fn) / 8.0, shape=(N, ), dtype=datatype, chunks="auto")
    z = da.fromfunction(lambda i: ((i + 1) / fn) / 9.0, shape=(N, ), dtype=datatype, chunks="auto")
    w = da.zeros(shape=(N, ), dtype=datatype, chunks="auto")
    x = da.zeros(shape=(N, ), dtype=datatype, chunks="auto")
    A.compute()
    u1.compute()
    u2.compute()
    v1.compute()
    v2.compute()
    y.compute()
    z.compute()
    w.compute()
    x.compute()
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    A += da.outer(u1, v1) + da.outer(u2, v2)
    x += beta * y @ A + z
    w += alpha * A @ x
    A.compute()
    x.compute()
    w.compute()
    compute_end = time.perf_counter()

    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }



def doitgen_bench(NR, NQ, NP, datatype):
    # init
    init_start = time.perf_counter()    
    A = da.fromfunction(lambda i, j, k: ((i * j + k) % NP) / NP, shape=(NR, NQ, NP), dtype=datatype, chunks="auto")
    C4 = da.fromfunction(lambda i,j: (i*j % NP) / NP, shape=(NP, NP, ), dtype=datatype, chunks="auto")
    sum = da.empty(shape=(NP, ), dtype=datatype, chunks="auto")
    A.compute()
    C4.compute()
    sum.compute()
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    A = da.reshape(da.reshape(A, (NR, NQ, 1, NP)) @ C4, (NR, NQ, NP))
    A.compute()
    compute_end = time.perf_counter()

    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }



def cholesky_bench(N, datatype):
    # init
    init_start = time.perf_counter()
    # TODO not doing this now because we don't have potrf ddace
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    # TODO this is already correct
    #A[:] = da.linalg.cholesky(A) + da.triu(A, k=1)
    #A.compute()
    compute_end = time.perf_counter()

    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }


def bicg_bench(M, N, datatype):
    # init
    init_start = time.perf_counter()
    A = da.fromfunction(lambda i,j: (i*(j+1)%N) / N, shape=(N, M), dtype=datatype, chunks="auto")
    p = da.fromfunction(lambda i: (i%M)/M, shape=(M, ), dtype=datatype, chunks="auto")
    r = da.fromfunction(lambda i: (i%N)/N, shape=(N, ), dtype=datatype, chunks="auto")
    s = da.zeros((M, ), dtype=datatype, chunks="auto")
    q = da.zeros((N, ), dtype=datatype, chunks="auto")
    A.compute()
    p.compute()
    r.compute()
    s.compute()
    q.compute()
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    s += r @ A
    q += A @ p
    s.compute()
    q.compute()
    compute_end = time.perf_counter()

    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }


def gemm_bench(I, J, K, datatype):
    # init
    init_start = time.perf_counter()
    A = da.fromfunction(lambda i, k: ((i * k + 1) % I) / I, shape=(I, K), dtype=datatype, chunks="auto")
    B = da.fromfunction(lambda k, j: ((k * j + 1) % K) / K, shape=(K, J), dtype=datatype, chunks="auto")
    C = da.fromfunction(lambda i, j: ((i * j + 1) % J) / J, shape=(I, J), dtype=datatype, chunks="auto")
    alpha = datatype(1.5)
    beta = datatype(1.2)
    A.compute()
    B.compute()
    C.compute()
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    C += alpha * A @ B + beta * C
    C.compute()
    compute_end = time.perf_counter()

    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }


def atax_bench(M, N, datatype):
    # init
    init_start = time.perf_counter()
    fn = datatype(N)
    A = da.fromfunction(lambda i, j: ((i + j) % N) / (5 * M), shape=(M, N), dtype=datatype, chunks="auto")
    x = da.fromfunction(lambda i: 1+(i/fn), shape=(N, ), dtype=datatype, chunks="auto")
    A.compute()
    x.compute() 
    init_end = time.perf_counter()

    # compute
    compute_start = time.perf_counter()
    res = (A @ x) @ A
    res.compute()
    compute_end = time.perf_counter()
    
    return { 'init': init_end-init_start, 'compute': compute_end-compute_start }


def res_to_str(d):
    s = ""
    # print standard arguments first
    s += str(d['framework'] + "     " +  d['kernel']) + "    " +  str(d['init']) + "   " + str(d['compute']) + "    " + str(d['workers']) + "    "
    del d['kernel']
    del d['framework'] 
    del d['init']
    del d['compute']
    del d['workers']
    # concatenate remaining args into a string
    s += "\""
    for k in d:
       s += str(k) + ":"
       if k == "datatype":
           s += str(d[k].__name__)
       else:
           s += str(d[k])
       s += ","
    s = s[:-1]
    s += "\""
    return s + "\n"



if __name__ == "__main__":

    print("Hello from DASK")
    runs = 7
    time.sleep(20)
    workers = len(client.scheduler_info()['workers'])
    for problem in sizes:
        for args in problem['arguments']:
            for a in args:
              scaler_func = problem['weak_scaling'].get(a, lambda x, procs:x)
              if type(args[a]) is int:
                args[a] = int(math.ceil(scaler_func(args[a], workers)))
              else:
                args[a] = scaler_func(args[a], workers)
            for run in range(runs):
                times = globals()[problem['kernel'] + "_bench"](**args)
                num_workers = {"framework": "DASK", "kernel" : problem['kernel'], "workers" : workers}
                print("Benchmarking "+problem['kernel']+" run "+str(run)+" of "+str(runs)+" using "+str(num_workers['workers'])+" workers.")
                times.update(args)
                times.update(num_workers)
                with open('dask.res', 'a') as f:
                    f.write(res_to_str(times))

        

