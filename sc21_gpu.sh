#!/bin/bash

frameworks=("dace_gpu")

benchmarks=("compute" "go_fast" "mandelbrot1")
deep_learning=("mlp" "resnet")
weather=("hdiff" "vadv")
polybench=("adi" "atax" "bicg" "cholesky" "correlation" "deriche"
           "fdtd_2d" "floyd_warshall" "gemm" "gemver"
           "gesummv" "heat_3d" "jacobi_1d" "jacobi_2d" "k2mm"
           "k3mm" "lu" "ludcmp" "mvt" "seidel_2d" "syrk" "trisolv")


for i in "${benchmarks[@]}"
do
    for j in "${frameworks[@]}"
    do
	    timeout 300s python npbench/benchmarks/$i/$i.py -f $j
    done
done

for i in "${deep_learning[@]}"
do
    for j in "${frameworks[@]}"
    do
	    timeout 300s python npbench/benchmarks/deep_learning/$i/$i.py -f $j
    done
done

for i in "${weather[@]}"
do
    for j in "${frameworks[@]}"
    do
	    timeout 300s python npbench/benchmarks/weather_stencils/$i/$i.py -f $j
    done
done

for i in "${polybench[@]}"
do
    for j in "${frameworks[@]}"
    do
	    timeout 500s python npbench/benchmarks/polybench/$i/$i.py -f $j
    done
done
